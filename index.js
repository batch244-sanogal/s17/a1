/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	
	//first function here:

	let myFirstname = prompt("What is your name?");
	let myAge = prompt ("How old are you?");
	let myLocation = prompt ("Where do you live?");
	
		console.log(myFirstname);
		console.log(myAge);
		console.log(myLocation);

	
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myBand() {
		console.log('1. Deftones');
		console.log('2. The Strokes');
		console.log('3. System of a down');
		console.log('4. Muse');
		console.log('5. The All American Reject');
	}

	myBand();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function myMov(){
		console.log('1. Fight Club');
		console.log('Rotten Tomatoes Rating: 79%');

		console.log('2. A Clockwork Orange');
		console.log('Rotten Tomatoes Rating: 88%');

		console.log('3. Pulp Fiction');
		console.log('Rotten Tomatoes Rating: 92%');

		console.log('4. Black Snake Moan');
		console.log('Rotten Tomatoes Rating: 68%');

		console.log('5. Archive');
		console.log('Rotten Tomatoes Rating: 78%');
	};

	myMov();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// let printFriends = function printUsers() {

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:"); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");

	// console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

// };
	


// console.log(friend1);
// console.log(friend2);